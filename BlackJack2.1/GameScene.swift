//
//  GameScene.swift
//  BlackJack2.1
//
//  Created by PWalker on 11/10/14.
//  Copyright (c) 2014 Paul Walker. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var chip:SKSpriteNode!
    var chip2:SKSpriteNode!
    
    var dragAndDrop:SKSpriteNode?
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        let myLabel = SKLabelNode(fontNamed:"Chalkduster")
        myLabel.text = "Hello, World!";
        myLabel.fontColor = UIColor.redColor()
        myLabel.fontSize = 65
        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        
        var backgroundTexture = SKTexture(imageNamed: "background")
        var background = SKSpriteNode(texture: backgroundTexture, size: CGSizeMake(CGRectGetWidth(self.frame), CGRectGetHeight(self.frame)))
        background.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame))
        
           //var chipTexture = SKTexture(imageNamed: "5DollarChip")
        chip = SKSpriteNode(imageNamed: "5DollarChip")
        chip.name = "5DollarChip"
        
        chip2 = SKSpriteNode(imageNamed: "25DollarChip")
        chip2.name = "25DollarChip"
        chip.xScale = 0.5
        chip.yScale = 0.5
        chip.position = CGPointMake(self.frame.width - (chip.frame.width/2) - 3, (chip.frame.height/2) + 5)
        chip2.position = CGPointMake(self.frame.width - (chip.frame.width/2) - chip.frame.width , (chip.frame.height/2) + 5)
        
      
        self.addChild(background)
        self.addChild(myLabel)
        self.addChild(chip)
        self.addChild(chip2)
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
        let location  = touches.anyObject()?.locationInNode(self)
        if chip.containsPoint(location!){
            dragAndDrop = SKSpriteNode(imageNamed:chip.name!)
            dragAndDrop?.position = location!
            addChild(dragAndDrop!)
        }
        
        //println("bchip = \(bchip)")
//        for touch: AnyObject in touches {
//            let location = touch.locationInNode(self)
        
//            let sprite = SKSpriteNode(imageNamed:"Spaceship")
//            
//            sprite.xScale = 0.5
//            sprite.yScale = 0.5
//            sprite.position = location
//            
//            let action = SKAction.rotateByAngle(CGFloat(M_PI), duration:1)
//            
//            sprite.runAction(SKAction.repeatActionForever(action))
//            
//            self.addChild(sprite)
//        }
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
        super.touchesMoved(touches, withEvent: event)
        let location  = touches.anyObject()?.locationInNode(self)
       
        if (dragAndDrop?.containsPoint(location!) != nil){
            dragAndDrop?.position = location!
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        super.touchesEnded(touches, withEvent: event)
        dragAndDrop = nil
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
